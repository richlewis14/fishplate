// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require twitter/bootstrap
//= require_tree .


// Function to fire the resize func every 1000ms
var resizeFunc = function(){
 $(window).resize(function(){
 if($(this).outerWidth() < 1200) {
  $('.span6.offset3.thickborder').removeClass().addClass('span6 offset3 formborder');
 }else{
  $('.span6.offset3.formborder').removeClass().addClass('span6 offset3 thickborder')
 }
});

};

setInterval(resizeFunc(), 1000);
$(document).resize(resizeFunc);

/*****************
   Flexslider
*****************/
var Main = Main || {};

    jQuery(window).load(function() {
      window.responsiveFlag = jQuery('#responsiveFlag').css('display');
      Main.gallery = new Gallery();
      
      jQuery(window).resize(function() {
        Main.gallery.update();
      });
    });

    function Gallery(){
      var self = this;
        container = jQuery('.flexslider'),
        clone = container.clone( false );
        
      this.init = function (){
        if( responsiveFlag == 'block' ){
          var slides = container.find('.slides');
          
          slides.kwicks({
            max : 500,
            spacing : 0
          }).find('li > a').click(function (){
            return false;
          });
        } else {
          container.flexslider();
        }
      }
      this.update = function () {
        var currentState = jQuery('#responsiveFlag').css('display');
        
        if(responsiveFlag != currentState) {
        
          responsiveFlag = currentState;
          container.replaceWith(clone);
          container = clone;
          clone = container.clone( false );
          
          this.init();  
        }
      }
      
      this.init();
    }