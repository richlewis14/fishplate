class NotificationsMailer < ActionMailer::Base
  default :from => "richlewis14@gmail.com"
  default :to => "richlewis14@gmail.com"

  def new_message(message)
    @message = message
    mail(:reason => "[YourWebsite.tld] #{message.reason}")
  end

end
