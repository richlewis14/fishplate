class Message 
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming



  attr_accessor :name, :email, :mobile, :reason, :comment
 
  validates_presence_of :name, :message => "Don't forget your name"
  validates_presence_of :reason, :message => "Please Select"
  validates_numericality_of :mobile, :message => "Is that right?"

  #validates_uniqueness_of :email  
 
  validates :name, :email, :mobile, :reason, :comment, :presence => true
  validates :email, :format => { :with => %r{.+@.+\..+} }, :allow_blank => true
  
  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end


  def persisted?
    false
  end

  

end

