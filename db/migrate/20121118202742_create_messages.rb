class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :name
      t.text :email
      t.integer :mobile
      t.string :reason
      t.text :comment

      t.timestamps
    end
  end
end
