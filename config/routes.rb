Fishplate::Application.routes.draw do
  
  scope :controller => :mains do
    get 'index'
    get 'about'
    get 'portfolio'
    get 'creative_minds'
    

  end

  


  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'mains#index'

  # See how all your routes lay out with "rake routes"

  match 'contact' => 'contact#new', :as => 'contact', :via => :get
  match 'contact' => 'contact#create', :as => 'contact', :via => :post

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
